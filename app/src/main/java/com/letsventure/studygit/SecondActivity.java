package com.letsventure.studygit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by abdul on 29/09/15.
 */
public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.second_activity);
    }
}
